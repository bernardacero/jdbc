package jdbc;

import java.sql.*;

public class CreateTable {

    static Connection connection = null;
    static Statement statement = null;
    static ResultSet resultSet = null;
    static String url = "jdbc:mysql://localhost:3306/sampledb";
    static String username = "root";
    static String password = "root";

    public static void main(String[] arg) throws SQLException {
        String tblName = "tbl5";
        String query = "CREATE TABLE " + tblName + "(id int primary key auto_increment not null,"
                + "name varchar(255), type varchar(255));";
        createTable(query);
    }

    public static void createTable(String query) throws SQLException {
        try {
            connection = DriverManager.getConnection(url, username, password);
            statement = connection.createStatement();

            boolean result = statement.execute(query);//false if it is update or no results 
            if (result == false) {
                System.out.println("NEW TABLE CREATED.");
                System.out.println(query);
            } else {
                System.out.println("FAILED TO CREATE NEW TABLE.");
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            connection.close();
        }
    }
}
