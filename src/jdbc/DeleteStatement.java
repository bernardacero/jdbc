package jdbc;

import java.sql.*;

public class DeleteStatement {

    static Connection connection = null;
    static Statement statement = null;
    static ResultSet resultSet = null;
    static String url = "jdbc:mysql://localhost:3306/sampledb";
    static String username = "root";
    static String password = "root";

    public static void main(String[] args) throws SQLException {
        int userid = 12;
        String query = "DELETE FROM USERS WHERE USERID = "+ userid;
        
        delete(query);
    }

    public static void delete(String query) throws SQLException {
        try {
            connection = DriverManager.getConnection(url, username, password);
            statement = connection.createStatement();
            int rowsAffected = statement.executeUpdate(query);

            if (rowsAffected > 0) {
                System.out.println(rowsAffected + " Row/s Deleted.");
            } else {
                System.out.println(rowsAffected + " Rows Affected. \n Deletion Failed.");
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            connection.close();
        }
    }
}
