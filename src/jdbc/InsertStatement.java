package jdbc;

import java.sql.*;

public class InsertStatement {

    static Connection connection = null;
    static Statement statement = null;
    static ResultSet resultSet = null;
    static String url = "jdbc:mysql://localhost:3306/sampledb";
    static String username = "root";
    static String password = "root";

    public static void main(String[] args) throws SQLException {

        String fname = "Melvin";
        String lname = "John";
        String insertQuery = "INSERT INTO USERS VALUES (NULL, '" + fname + "', '" + lname + "');";

        insert(insertQuery);
    }

    public static void insert(String query) throws SQLException {
        try {
            connection = DriverManager.getConnection(url, username, password);
            statement = connection.createStatement();
            int row = statement.executeUpdate(query);//RETURN AFFECTED ROWS

            if (row > 0) {
                System.out.println("INSERT QUERY SUCCESSFUL!");
                System.out.println("QUERY : " + query);
            } else {
                System.out.println("INSERT QUERY FAILED!");
            }
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            connection.close();
        }
    }
}
