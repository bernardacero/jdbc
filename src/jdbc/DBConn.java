package jdbc;

import java.sql.*;

public class DBConn {
    public static Connection conn = null;
    public static Statement st = null;
    public static PreparedStatement pst = null;
    public static ResultSet rs = null;
    
    private static String url = "jdbc:mysql://localhost:3306/sampledb";
    private static String username = "root";
    private static String password = "root";
    
    public static void openConnection(){
        try{
            //Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, password);
            st = conn.createStatement();
        }catch(Exception e){
            System.err.println(e);
        }
    }
    
    public static void closeConnection(){
        try{
            conn.close();
        }catch(Exception e){
            System.err.println(e);
        }
    }
    
}
