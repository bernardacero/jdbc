package jdbc;

import java.sql.*;
import java.util.Arrays;

public class BatchUpdate {

    public static String url = "jdbc:mysql://localhost:3306/sampledb";
    public static String username = "root";
    public static String password = "root";

    public static void main(String[] args) {
        batchUpdatePreparedStatement();
    }

    private static void batchUpdate() {
        //Establish connection
        try ( Connection connection = DriverManager.getConnection(url, username, password);  
            Statement statement = connection.createStatement()) {
                //disable autocommit;
                connection.setAutoCommit(false);
                //add batch statement
                statement.addBatch("UPDATE USERS SET FIRSTNAME = 'Sam' WHERE USERID = 1;");
                statement.addBatch("UPDATE USERS SET LASTNAME = 'Speres' WHERE USERID = 17;");
                //exetue batch and return int array
                int count[] = statement.executeBatch();
                System.out.println(Arrays.toString(count));
                //commit connection
                connection.commit();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
     private static void batchUpdatePreparedStatement() {
         String query= "UPDATE USERS SET FIRSTNAME = ? WHERE USERID = ?";
         
        //Establish connection
        try ( Connection connection = DriverManager.getConnection(url, username, password);  
            PreparedStatement statement = connection.prepareStatement(query)) {
            
                //disable autocommit;
                connection.setAutoCommit(false);
                
                //add batch statement
                statement.setString(1, "Benj");
                statement.setInt(2, 2);
                statement.addBatch();
                
                statement.setString(1, "Leonard");
                statement.setInt(2, 6);
                statement.addBatch();
                
                //exetue batch and return int array
                int count[] = statement.executeBatch();
                System.out.println(Arrays.toString(count));
                
                //commit connection
                connection.commit();
                connection.setAutoCommit(true);
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
}

