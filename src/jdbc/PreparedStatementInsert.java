package jdbc;

import java.sql.*;

public class PreparedStatementInsert {

    static Connection connection = null;
    static PreparedStatement statement = null;
    static ResultSet resultSet = null;
    static String url = "jdbc:mysql://localhost:3306/sampledb";
    static String username = "root";
    static String password = "root";

    public static void main(String[] args) throws SQLException {
        String fname = "Jasmin";
        String lname = "Blue";
        String query = "INSERT INTO USERS(FIRSTNAME, LASTNAME) VALUES(?,?);";
        
        insert(query, fname, lname);
    }
    
    public static void insert(String query, String fname, String lname) throws SQLException{
        try{
            connection = DriverManager.getConnection(url, username, password);
            statement = connection.prepareStatement(query);
            //Setting placeholder values
            statement.setString(1, fname);
            statement.setString(2, lname);
            //printing out prepared statement
            System.out.println(statement);
            //check if it return affected row.
            int row = statement.executeUpdate();//return 0 if failed
            if(row > 0){
                System.out.println("INSERTION SUCCESSFULL.");
            }else{
                System.out.println("INSERTION FAILED.");
            }
        }catch(SQLException e){
            System.out.println(e);
        }finally{
            connection.close();
        }
    }
}
