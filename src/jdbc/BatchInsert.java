package jdbc;

import java.sql.*;
import java.util.Arrays;

public class BatchInsert {

    public static String url = "jdbc:mysql://localhost:3306/sampledb";
    public static String username = "root";
    public static String password = "root";

    public static void main(String[] args) throws SQLException {
       // batchInsertStatement();
        batchInsertPreparedStatement();
    }

    private static void batchInsertStatement() {
        //Establish connection and create statement using try-with-resourses to auto close connection
        try ( Connection connection = DriverManager.getConnection(url, username, password);  Statement statement = connection.createStatement()) {
            
            connection.setAutoCommit(false);//to make insert batch as single transaction
       
            //Add batch qeuries
            statement.addBatch("INSERT INTO USERS VALUES(null, 'Gord','Myers');");
            statement.addBatch("INSERT INTO USERS VALUES(null, 'Alicia','Cuffin');");
            statement.addBatch("INSERT INTO USERS VALUES(null, 'Derek','Stain');");
            statement.addBatch("INSERT INTO USERS VALUES(null, 'Edward','Hands');");
            statement.addBatch("INSERT INTO USERS VALUES(null, 'William','Ollie');");
            //Execute batch statement
            int batchCount[] = statement.executeBatch();
            System.out.println(Arrays.toString(batchCount));
            connection.commit();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    public static void batchInsertPreparedStatement(){
        String query = "INSERT INTO USERS (FIRSTNAME, LASTNAME) VALUES(?, ?);";
        try(Connection connection = DriverManager.getConnection(url, username, password); 
            PreparedStatement preparedstatement = connection.prepareStatement(query)){
                //set batch as one transaction
                connection.setAutoCommit(false);
                
                preparedstatement.setString(1, "Tony");
                preparedstatement.setString(2, "Storks");
                preparedstatement.addBatch();
                
                preparedstatement.setString(1, "Jessie");
                preparedstatement.setString(2, "Joe");
                preparedstatement.addBatch();
                
                preparedstatement.setString(1, "Antonette");
                preparedstatement.setString(2, "Lee");
                preparedstatement.addBatch();
                
                //execute batch
                int count[] = preparedstatement.executeBatch();
                System.out.println(Arrays.toString(count));
                
                connection.commit();
                connection.setAutoCommit(true);
        }catch(SQLException e){
            System.out.println(e);
        }
    }
}
    