package jdbc;

import java.sql.*;

public class PreparedStatementSelect {

    static Connection connection = null;
    static PreparedStatement statement = null;
    static ResultSet resultSet = null;
    static String url = "jdbc:mysql://localhost:3306/sampledb";
    static String username = "root";
    static String password = "root";

    public static void main(String[] args) throws SQLException {
        int userid = 87;
        String query = "SELECT * FROM USERS WHERE USERID = ?";

        select(query, userid);
    }

    public static void select(String query, int id) throws SQLException {
        try {
            connection = DriverManager.getConnection(url, username, password);
            statement = connection.prepareStatement(query);
            //set parameters
            statement.setInt(1, id);

            resultSet = statement.executeQuery();
            System.out.println(statement);
            //process result set
            while (resultSet.next()) {
                int uid = resultSet.getInt("userid");
                String fname = resultSet.getString("Firstname");
                String lname = resultSet.getString("Lastname");
                System.out.println("userid: " + uid + "\nfirsname: " + fname + "\nlastname: " + lname);
                System.out.println("");
            }

        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            connection.close();
        }
    }
}
