package jdbc;

import java.sql.*;

public class Select{
    
    public static void main(String[] args) throws SQLException{
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String url = "jdbc:mysql://localhost:3306/sampledb";
        String username = "root";
        String password = "root";
        String selectQuery = "SELECT * FROM USERS";
        
        try{
            connection = DriverManager.getConnection(url, username, password);
            preparedStatement = connection.prepareStatement(selectQuery);
            resultSet = preparedStatement.executeQuery();
            
            while(resultSet.next()){
                int uid = resultSet.getInt("UserID");
                String fname = resultSet.getString("Firstname");
                String lname = resultSet.getString("Lastname");
                
                System.out.println("UserID : "+ uid +"\nFirstname : " + fname + "\nLastname : " + lname);
                System.out.println("");
            }
        }catch(SQLException e){
            System.out.println(e);
        }finally{
            connection.close();
        }
    }
}