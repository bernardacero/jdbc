package jdbc;

import java.sql.*;

public class PreparedStatementUpdate {
    static Connection connection = null;
    static PreparedStatement statement = null;
    static ResultSet resultSet = null;
    static String url = "jdbc:mysql://localhost:3306/sampledb";
    static String username = "root";
    static String password = "root";

    public static void main(String[] args) throws SQLException {
        int userid = 5;
        String fname = "Larry";
        String lname = "White";
        String query = "UPDATE USERS SET FIRSTNAME = ?, LASTNAME = ? WHERE USERID = ?";

        update(query, fname, lname, userid);
    }
    
    public static void update(String query, String fname, String lname, int userid) throws SQLException{
        try{
            connection = DriverManager.getConnection(url, username, password);
            statement = connection.prepareStatement(query);
            //Setting placeholder or ? in the query
            statement.setString(1, fname);
            statement.setString(2, lname);
            statement.setInt(3, userid);
          
            System.out.println(statement);
            
            int row = statement.executeUpdate();
            if(row > 0){
                System.out.println("Update successful.");
            }else{
                System.out.println("Update failed.");
            }
            
        }catch(SQLException e){
            System.out.println(e);
        }finally {
            connection.close();
        }
    }
}
