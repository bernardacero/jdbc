package jdbc;

import java.sql.*;

public class Transaction {
    private static String url = "jdbc:mysql://localhost:3306/sampledb";
    private static String username = "root";
    private static String password = "root";
    
    private static String insertQuery = "INSERT INTO USERS (FIRSTNAME, LASTNAME) VALUES(?,?);";
    private static String updateQuery = "UPDATE USERS SET LASTNAME = ? WHERE USERID = ?";
    
    public static void main(String[] args){
        //SET CONNECTION TO AUTO CLOSE
        //CREATE DATABASE CONNECTION
        try(Connection conn = DriverManager.getConnection(url, username, password)){
            //SET AUTO COMMIT TO FALSE
            conn.setAutoCommit(false);
            //SET PREPARED STATEMENT TO AUTO CLOSE
            try(PreparedStatement insert = conn.prepareStatement(insertQuery);
                PreparedStatement update = conn.prepareStatement(updateQuery)){
                
                //CREATE INSERT STATEMENT
                insert.setString(1, "Tom");
                insert.setString(2, "Holland");
                insert.executeUpdate();
                
                //CREATE UPDATE STATEMENT
                update.setString(1, "Travis");
                update.setInt(2, 2);
                update.executeUpdate();
                
                //commit update and insert
                conn.commit();                
            }catch(SQLException e){
                e.printStackTrace();
                if(conn != null){
                    try{
                        System.out.println("Transaction is rolling back.");
                        conn.rollback();
                    }catch(SQLException ex){
                        ex.printStackTrace();
                    }
                }
            }
        }catch(SQLException e){
            System.out.println(e);
        }
    }
}
