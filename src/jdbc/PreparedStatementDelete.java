package jdbc;

import java.sql.*;

public class PreparedStatementDelete {

    static Connection connection = null;
    static PreparedStatement statement = null;
    static ResultSet resultSet = null;
    static String url = "jdbc:mysql://localhost:3306/sampledb";
    static String username = "root";
    static String password = "root";
    
    public static void main(String[] args) throws SQLException{
        int userid = 13;
        String query ="DELETE FROM USERS WHERE USERID = ?";
        
        delete(query, userid);
    }
    
    public static void delete(String query, int userid) throws SQLException{
        try{
            connection = DriverManager.getConnection(url, username, password);
            statement = connection.prepareStatement(query);
            //setting placeholder
            statement.setInt(1, userid);
            
            int row = statement.executeUpdate();
            if(row > 0){
                System.out.println(row+" ROW/S DELETED.");
            }else{
                System.out.println("DELETION FAILED.");
            }
        }catch(SQLException e){
            System.out.println(e);
        }finally{
            connection.close();
        }
    }
}
