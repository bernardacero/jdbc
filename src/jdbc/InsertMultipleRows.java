package jdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class InsertMultipleRows {
    static String url = "jdbc:mysql://localhost:3306/sampledb";
    static String username = "root";
    static String password = "root";
    public static void main(String[] args){
        List<User> list = new ArrayList<>();
        list.add(new User("Joana", "Charles"));
        list.add(new User("Vince", "Barkley"));
        list.add(new User("Melody", "Lucas"));
        
        String query ="INSERT INTO USERS VALUES (NULL,?,?);";
        
        try(Connection conn = DriverManager.getConnection(url, username, password);
            PreparedStatement pst = conn.prepareStatement(query)){
            conn.setAutoCommit(false);
            for (Iterator<User> iterator = list.iterator(); iterator.hasNext();) {
                User user = (User) iterator.next();
                pst.setString(1, user.getFname());
                pst.setString(2, user.getLname());
                pst.addBatch();
            }
            int counts [] = pst.executeBatch();
            System.out.println(Arrays.toString(counts));
            
            conn.commit();
            conn.setAutoCommit(true);
        }catch(SQLException e){
            System.out.println(e);
        }
    }
}
